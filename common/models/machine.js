'use strict';

let fs = require('fs');
let multer = require('multer');
let Excel = require('exceljs');

module.exports = function(Machine) {

  Machine.remoteMethod('uploadExcel', {
    http: {
      verb: 'post',
      path: '/upload',
    },
    accepts: [{
      arg: 'req',
      type: 'object',
      http: {
        source: 'req',
      },
    }, {
      arg: 'res',
      type: 'object',
      http: {
        source: 'res',
      },
    }],
    returns: {
      arg: 'data',
      type: 'string',
    },
  });

  Machine.remoteMethod('getExcel', {
    http: {
      verb: 'get',
      path: '/excel'
    },
    accepts: {
      arg: 'res',
      type: 'object',
      http: {
        source: 'res',
      },
    },
    returns: {
      'file': {type: 'file'}
    }
  });

  let uploadedFileName = '';
  let storage = multer.diskStorage({
    destination: function(req, file, cb) {
      let dirPath = 'client/uploads/';
      if (!fs.existsSync(dirPath)) {
        let dir = fs.mkdirSync(dirPath);
      }
      cb(null, dirPath + '/');
    },
    filename: function(req, file, cb) {
      // file will be accessible in `file` variable
      let ext = file.originalname.substring(file.originalname.lastIndexOf("."));
      let fileName = Date.now() + ext;
      uploadedFileName = fileName;
      cb(null, fileName);
    }
  });

  Machine.uploadExcel = function(req, res, cb){
    let upload = multer({
      storage: storage,
    }).array('file', 12);
    upload(req, res, function(err) {
      if (err) return res.json(err);
      produceExcel(uploadedFileName);
      return res.json(uploadedFileName);
    });
  };

  Machine.getExcel = async function(res){
    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet('Machines');

    let data = await Machine.find({});

    worksheet.columns = [
      {header: 'Machine', key: 'machine'},
      {header: 'attribute', key: 'attribute'},
      {header: 'Reading', key: 'reading'}
      ];

    for (let i = 0; i < data.length; i++) {
      data[i].__data
      worksheet.addRow({machine: data[i].__data.machine, attribute: 'position', reading: data[i].__data.position});
      worksheet.addRow({machine: data[i].__data.machine, attribute: 'Closing time', reading: data[i].__data.closing});
      worksheet.addRow({machine: data[i].__data.machine, attribute: 'active', reading: data[i].__data.active});
    }

    let fileName = `${Date.now()}export.xlsx`;
    await workbook.xlsx.writeFile(`client/uploads/${fileName}`);

    let file = `client/uploads/${fileName}`;
    return res.download(file);
  };

  async function produceExcel(file) {
    let workbook = new Excel.Workbook();
    await workbook.xlsx.readFile('client/uploads/' + file);
    let worksheet = workbook.worksheets[0];
    let data = {};

    for (let i = 2; i <= worksheet._rows.length; i++) {
      let row = worksheet.getRow(i).values;

      if (!data[row[1]]) data[row[1]] = {};
      if (row[2] === 'position') data[row[1]].position = row[3];
      if(row[2] === 'Closing time') data[row[1]].closing = row[3];
      if(row[2] === 'active') data[row[1]].active = row[3];
    }

    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let exists = await Machine.findOne({where: {machine: +keys[i]}});
      if (exists){
        let update = {};
        if (data[keys[i]].position) update.position = data[keys[i]].position;
        if (data[keys[i]].closing) update.closing = data[keys[i]].closing;
        if (data[keys[i]].active) update.active = data[keys[i]].active;
        await exists.save(update);
      }
      else{
        if (!!data[keys[i]].position || !!data[keys[i]].closing || !!data[keys[i]].active) continue;
        await Machine.create({
          machine: +keys[i],
          position: data[keys[i]].position,
          closing: data[keys[i]].closing,
          active: data[keys[i]].active
        }, function(err) {
          if (err)
            console.log(`Error during adding string: ${err.toString()}`);
        })
      }
    }
  }
};
