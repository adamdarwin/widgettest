'use strict';

var server = require('./server');
var models = require('./model-config');

var ds = server.dataSources.psql;
var lbTables = Object.keys(models).filter(key => key !== '_meta');
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  ds.disconnect();
});
